
# sed, grep, egrep, cat, tac, tail, head, less

## sed

SED это редактор потока.

` sed <команда> <файл> `

-i - редактировать в файле (без ключа вывоедет в stdout результат редактирования)

## grep 

GREP расшифровывается как «global regular expression print» (то имеется, «искать везде соответствующие постоянному выражению строки и выводить их»).

` grep <регулярное выражение> <файл для поиска> `

-h	- выводит с именем файла  
-l	- выдает только имена файлов с найденным выражением (без повторений)
-b	- выводит с номером строки (начиная с 0)
-n	- выводит с номером строки (начиная с 1)
-c	- выдает только количество строк с найденным выражением
-i	- игнорирует регистр при сравнении
-s	- не выводит ошибки чтения файлов
-v	- выдает инверсивный поиск (все, кроме совпадений)
-w	- ищет выражение как слово (разделенное пробелами, например)
-F	- ищет выражение как строку
-e <список_образцов>	- задает несколько образцов для поиска (можно оставить один)
-E	- расширяет возможность описывать регулярное выражение
-f <файл_образцов> - читает регулярные выражения из файла образцов
-q	- тихий режим, ничего не выводится кроме подходящих строк

## egrep

EGREP - это урезанный вызов grep c ключом -E

## cat

CAT - объединяет файлы и направляет их на стандартный вывод.

` cat <files> `

-b, --number-nonblank - нумерует все непустые строки выходного файла, начиная c 1
-n, --number - нумерует все строки выходного файла, начиная с 1
-s, --squeeze-blank - заменяет набор пустых строк одной пустой строкой
-E, --show-ends - выводит в конце каждой строки символ $
-v, --show-nonprinting - выводит управляющие символы кроме символов перевода строки (LFD) и табуляции (TAB), для которых использует символ ^ и M-нотацию
-e - эквивалентно -vE
-T, --show-tabs - выводит символы табуляции в формате ^I
-t - эквивалентно -vT

## tac

Это cat который выводит фалы наоборот (например, в логах будет самое раннее наверху)

## tail

Выводит N строк конца

-n - количество строк (по умолч 10)

## head

Выводит N строк начала

-n - количество строк (по умолч 10)

## less

Позволяет просматривать файл целиком
