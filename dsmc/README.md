## dsmc

Просмотр расписания

` sudo dsmc q sched `

Инкриментный бекап

` sudo dsmc incr "/pathtofile/*" -su=yes `

Просмотр списка файлов в бекапе

` sudo dsmc q ba "/pathtofile/*" -su=yes `

Востановление последнего бекапа

` sudo dsmc rest "/pathtofile/*" "/pathtofile/" -su=yes `

Востановление конкретного файла

` sudo dsmc rest "/pathtofile/file" "/path/file" `
