## openstack

Авторизуемся в openstack с помощь файлика авторизации (находится в вебе)
source file-openrc.sh < <(echo пароль от опенстека) 

### Основные команды

Список серверов

` nova list или openstack server list --long `

Список дисков

` openstack volume list `

Создание вольюма

` openstack volume create --size 4000 --availability-zone zona --type ssd name-1 `

Создание ВМ

` openstack server create --flavor 4cpu8ram100hdd --image <id_image> --nic net-id=<id_net> --key-name=<key> name_server `

Добавление вольюма

` openstack server add volume name_server name-1 `

Удаление вольюма

` openstack volume delete name-vol `

Отключение вольюмов от ВМ

` openstack server remove volume name_server name-1`

Доступные зоны 

` openstack availability zone list `

Доступные образы

` openstack image list `

Удалить сервер

` openstack server delete newServer `

Посмотреть список доступных ключей

` openstack keypair list `

Добавить ключ

` openstack keypair create [--public-key <file> | --private-key <file>] <name> `

Посмотреть квоту 

` openstack quota show `

Посмотреть группы серверов

` openstack server group list `

Добавить группу серверов

` openstack server group create --policy <policy> <name> `

Удалить группу серверов

` openstack server group delete `

Посмотреть список флейворов

` openstack flavor list `
