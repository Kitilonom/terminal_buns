## Journalctl

# Изменить размер журнала

Проверим какой объем журнал занимает в данный момент

` sudo journalctl --disk-usage `

Перед уменьшением упорядочиваем записи в журнале

` sudo journalctl --rotate `

Настраиваем ротацию по времени или месту или количеству файлов

` sudo journalctl --vacuum-time=1s `

` journalctl --vacuum-size=500M `

` sudo journalctl --vacuum-files 1 `
