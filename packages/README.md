
# PACKAGES

## Apt && Apt-get

APT (Advanced Packaging Tool) — набор утилит для работы с пакетами в Linux, а также для управления репозиториями. Это так называемый пакетный менеджер.

Список репозиториев можно найти в файле `/etc/apt/sources.list`, а также в директории `/etc/apt/sources.list.d/`.

Apt это новая версия apt-get.

` sudo apt <действие> <пакет(ы)> <доп. параметры> `

` sudo apt upgrade ` - обновление пакетов, установленных в системе. Данная команда только обновляет пакеты до новых версий, но никогда не удаляет и не устанавливает другие пакеты.

` sudo apt full-upgrade ` - обновление пакетов. Также удаляет или устанавливает новые пакеты, если это потребуется для разрешения зависимостей.

` sudo apt install <пакет1> <пакет2> ` - установить пакет

` sudo apt install <пакет>=<версия> ` - установить пакет определенной версии

` sudo apt remove <пакет> ` - удалить пакет

` sudo apt purge <пакет> ` - удалить пакет и его конфигурационные файлы

` sudo apt autoremove ` - удалить неиспользуемые пакеты

` apt list --installed ` -  вывести список установленных в системе пакетов

` apt list --upgradable ` - вывести список пакетов, которые требуют обновления (у которых вышла новая версия)

` apt list --all-versions ` - вывести список всех пакетов доступных для вашей системы

` apt search <слово> ` - поиск указанного слова в названии пакетов и в описании пакетов. Поддерживаются регулярные выражения

` apt search --names-only <слово> ` - поиск только по названиям пакетов

` apt show <пакет> ` - информацию о пакете. Отображается версия, размер, описание, зависимости и другая информация

` sudo apt edit-sources ` - открывает файл /etc/apt/sources.list в текстовом редакторе для редактирования, после сохранения изменений и закрытия редактора, выполняет проверку файла на предмет ошибок. В случае наличия ошибок, выводит предложение на повторное редактирование файла, чтобы исправить ошибки.

## Snap

Снап можно считать таким пакетом, в котором содержится не только программа, но и все необходимые для ее работы библиотеки. Получается, что все, что нужно для работы программы содержится в снапе.

` sudo apt install snapd ` - установка snap

` sudo snap find <название> ` - установка пакета

` sudo snap refresh ` - обновление всех установленных пакетов

` sudo snap refresh <пакет> ` - обновление одного пакета

` sudo snap remove ` - удаление пакета

` sudo snap list ` - просмотр списка установленных пакетов

` sudo snap find ` - для поиска пакета (поиск по имени И описанию)

` sudo snap info <пакет> ` - информация о каком-либо пакете

` sudo snap revert <пакет> ` - откат обновления

` sudo snap disable <пакет> ` - временно отключает пакет

` sudo snap enable <пакет> ` - делает пакет обратно доступным

` sudo snap run <пакет> ` - запуск пакета (или через граф меню ярлык нажать)

## Dpkg

Это пакетный менеджер для дебиан систем который работает с локально установленными пакетами.

` sudo dpkg -l ` - получение списка пакетов, установленных в системе

` sudo dpkg -L <package> ` - получение списка файлов, установленных пакетом

` sudo dpkg -S <file> ` - посмотреть к какому пакеты относится конкретный файл

` sudo dpkg -i <package.deb> ` - установить локальный .deb файл

` sudo dpkg -r <package> ` - удаление пакета

## Tar

Так, чтобы создать новый архив, в терминале используется следующая конструкция:

` tar опции архив.tar файлы_для_архивации `

Для его распаковки:

` tar опции архив.tar `

-c – создать (create) новый архив

-x – извлечь (extract) файлы из архива

–delete – удалить (delete) файлы из архива

-r – добавить (append) файлы в существующий архив

-A – добавить (append) tar-файлы в существующий архив

-t – список файлов в архиве (содержимое архива)

-u – обновить (update) архив

-d – операция сравнения архива с заданной файловой системой

-z – обработка архива с помощью gzip. Сжатие или разжатие, в зависимости от комбинации сопутствующих ключей -c или -x.

-j – обработка архива с помощью bzip2. Сжатие или разжатие, в зависимости от комбинации сопутствующих ключей -c или -x.

-f - вывод информация в файл. Без этого ключа tar будет выдавать результат на stdout при упаковке и пытаться читать архив с stdin при распаковке.

-v — включает визуальное отображение процесса архивации

